var map;
function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 4
  });
}
var button = document.getElementById("button");
var ip = document.getElementById("ip");
var table = document.getElementById("table");
var file = document.getElementById("file");
var ips = ['49.199.49.26', '190.165.20.142'];
var mapMarkers = [];

button.onclick = getLocation;
file.onchange = readFile;

function getLocation(){
    let url = `http://api.ipstack.com/${ip.value}?access_key=5d567732c544bea5eadcb74d041bb3e4`;
    fetch(url)
    .then(response => response.json())
    .then(data => {
        //addMarker(data);
        mapMarkers.push({latLng: [data.latitude, data.longitude], name: data.city})
        map.addMarkers(mapMarkers, []);
        addTable(data);
    })
    .catch(error => console.error(error));
}

function getLocation2(ip){
    let url = `http://api.ipstack.com/${ip}?access_key=5d567732c544bea5eadcb74d041bb3e4`;
    fetch(url)
    .then(response => response.json())
    .then(data => {
        //addMarker(data);
        (Array.isArray(data))
        ? data.forEach( e => mapMarkers.push({latLng: [e.latitude, data.longitude], name: e.city}) )
        : mapMarkers.push( {latLng: [data.latitude, data.longitude], name: data.city});
        
        map.addMarkers(mapMarkers, []);
        addTable(data);
    })
    .catch(error => console.error(error));
}

function addTable({location,ip}) {
    let row = table.insertRow(1);
    let cell1 = row.insertCell(0);
    cell1.innerHTML = `${ip} ${location.country_flag_emoji}`;   
}

function addMarker({latitude,longitude,ip}) {
    let location = {lat:latitude,lng:longitude};
    var marker = new google.maps.Marker({
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: location,
        map: map,
        title: ip,
        icon:"https://img.icons8.com/officexs/28/000000/person-female.png",
    });
    map.setCenter(location);
    map.setZoom(12);
}

map = new jvm.Map({
    container: $('#map'),
    map: 'world_mill_en',
    scaleColors: ['#C8EEFF', '#0071A4'],
    normalizeFunction: 'polynomial',
    hoverOpacity: 0.7,
    hoverColor: false,
    markerStyle: {
      initial: {
        fill: '#F8E23B',
        stroke: '#383f47'
      }
    },
    backgroundColor: '#383f47',
    markers: mapMarkers
  });
  
// cargar las ips en el array 
//ips.forEach(ip=>getLocation2(ip));

function ExcelToJSON(file) {

  var reader = new FileReader();

  reader.onload = function(e) {
      var data = e.target.result;
      var workbook = XLSX.read(data, {
          type: 'binary'
      });

      var json_object;

      workbook.SheetNames.forEach(function(sheetName) {
          json_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
      })

      let List = Object.keys(json_object[0]);
      json_object.forEach( (Element,index) => List.push( Element[ Object.keys( json_object[ index ])]))
      stringIps = List.join(",");
      //getLocation2(stringIps);
      stringIps.split(",").forEach(ip=>getLocation2(ip));
  };

  reader.onerror = function(ex) {
      console.log(ex);
  };

  reader.readAsBinaryString(file);
};

function readFile(evt) {
  var files = evt.target.files; // FileList object

  // use the 1st file from the list
  f = files[0];
  ExcelToJSON(f);
}
